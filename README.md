# rename_img

Renames the name of the image and its file creation date by the date the picture was taken.

The script must be placed in the root of the folders or files


## Launch 
```
python3 rename_img.py
```


## Install 
```
virtualenv -p python3 <name>
source <name>/bin/activate
pip install -r requirements.txt
```
