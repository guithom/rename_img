import os
from datetime import datetime

from PIL import Image
from PIL.ExifTags import TAGS


def get_datetime(date_time: str) -> datetime:
    return datetime.strptime(date_time, "%Y:%m:%d %H:%M:%S")


def convert_date(start_date_string: str) -> str:
    # 2018:09:28 12:44:22 -> 2018-09-28_12:44:22
    try:
        date_datetime = get_datetime(start_date_string)
        end_date_string = date_datetime.strftime("%Y-%m-%d_%H:%M:%S")
        return end_date_string
    except Exception as e:
        print(f"Error during the convertion of the date: {e}")


def set_mtime(file_path: str, date_time: str) -> None:
    stat = os.stat(file_path)
    mtime = get_datetime(date_time)
    os.utime(file_path, times=(stat.st_mtime, mtime.timestamp()))


def rename_img(old_path: str, new_path: str) -> None:
    os.rename(old_path, new_path)


def get_dateTime_to_img(image_path: str) -> str:
    # read the image data using PIL
    image = Image.open(image_path)
    # extract EXIF data
    exifdata = image.getexif()
    # iterating over all EXIF data fields
    for tag_id in exifdata:
        # get the tag name, instead of human unreadable tag id
        tag = TAGS.get(tag_id, tag_id)
        data = exifdata.get(tag_id)
        # decode bytes
        if isinstance(data, bytes):
            data = data.decode()
        # print(f"{tag:25}: {data}")

        if tag == "DateTimeOriginal":
            return data


def process(root: str, filename: str) -> bool:
    if filename.endswith((".JPEG", ".JPG", ".jpg", ".jpeg")):
        _, ext = os.path.splitext(filename)
        print(f"File converted: {filename}")
        file_path = os.path.join(root, filename)
        dt = get_dateTime_to_img(file_path)

        if dt is not None:
            new_dt = convert_date(dt)
            new_path = os.path.join(root, new_dt + ext)
            rename_img(file_path, new_path)
            print(f"File converted: {filename} -> {new_dt + ext}")
            set_mtime(new_path, dt)

            return True
        return False


def run():
    path = os.getcwd()
    files_failed = []

    for (root, dirs, file) in os.walk(path):
        for f in file:
            rst = process(root, f)
            if not rst:
                files_failed.append(f)

    print(f"file failed: {files_failed}")


if __name__ == "__main__":
    run()
